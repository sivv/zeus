unit zeus.zen.commandline;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections;

type
  TCommandHandler = class(TObject)
  private
    FParams: TArray<string>;
  public
    constructor Create(Params : TArray<string>);
    property Params : TArray<string> read FParams;
    function Help : string; virtual; abstract;
    function Description : string; virtual; abstract;
    procedure Execute; virtual; abstract;
  end;
  TCommandHandlerClass = class of TCommandHandler;

  TCommandLine = class
  private
    class var FCommands : TDictionary<string, TCommandHandlerClass>;
  public
    class procedure Execute;
    class procedure RegisterCommand(Command : string; HandlerClass : TCommandHandlerClass);
    class constructor Create;
    class destructor Destroy;
  end;

implementation

{ TCommandLine }

class constructor TCommandLine.Create;
begin
  FCommands := TDictionary<string, TCommandHandlerClass>.Create;
end;

class destructor TCommandLine.Destroy;
begin
  FCommands.Free;
end;

class procedure TCommandLine.Execute;
var
  ary : TArray<string>;
begin
  if ParamCount() = 0 then
  begin
    WriteLn('zen.exe requires a valid action to be supplied.  Known actions are:');
    WriteLn;
    var sl := TStringList.Create;
    try
      sl.Sorted := True;
      for var p in FCommands do
      begin
        sl.Add(p.Key);
      end;

      setlength(ary,0);
      for var i := 0 to sl.count-1 do
      begin
        var cmd : TCommandHandler := FCommands[sl[i]].Create(ary);
        try
          WriteLn(#9+sl[i]+#9+cmd.Description);
        finally
          cmd.Free;
        end;
      end;
    finally
      sl.Free;
    end;
    Readln;
  end else
  begin
    if FCommands.ContainsKey(ParamStr(1).ToLower) then
    begin
      SetLength(ary, ParamCount()-1);
      for var i := 2 to ParamCount() do
        ary[i-2] := ParamStr(i);

      var cmd := FCommands[ParamStr(1).ToLower].Create(ary);
      try
        WriteLn('Executing '+ParamStr(1).ToLower);
        try
          cmd.Execute;
        except
          on e: exception do
          begin
            Write('ERROR: '+e.ClassName+' "'+e.Message+'"');
          end;
        end;
      finally
        cmd.Free;
      end;
    end;
  end;
end;

class procedure TCommandLine.RegisterCommand(Command: string;
  HandlerClass: TCommandHandlerClass);
begin
  FCommands.AddOrSetValue(Command.ToLower, HandlerClass);
end;

{ TCommandHandler }

constructor TCommandHandler.Create(Params: TArray<string>);
begin
  inherited Create;
  FParams := Params;
end;

end.
