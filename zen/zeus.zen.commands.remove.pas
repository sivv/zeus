unit zeus.zen.commands.remove;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenRemoveCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenRemoveCmd }

function TZenRemoveCmd.Description: string;
begin
  Result := 'Specify a module to remove from your project';
end;

function TZenRemoveCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('remove', TZenRemoveCmd);


end.
