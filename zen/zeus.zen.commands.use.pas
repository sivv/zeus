unit zeus.zen.commands.use;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenUseCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenUseCmd }

function TZenUseCmd.Description: string;
begin
  Result := 'Specify a module to use in your project';
end;

function TZenUseCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('use', TZenUseCmd);

end.
