unit zeus.zen.commands.learn;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenLearnCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenLearnCmd }

function TZenLearnCmd.Description: string;
begin
  Result := 'Add a private or public git repo to Zen''s directory allowing it to be used in the current or future projects.';
end;

function TZenLearnCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('learn', TZenLearnCmd);




end.
