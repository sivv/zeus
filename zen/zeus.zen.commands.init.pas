unit zeus.zen.commands.init;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenInitCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenUseCmd }

function TZenInitCmd.Description: string;
begin
  Result := 'Initialize a project for use with zen';
end;

function TZenInitCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('init', TZenInitCmd);

end.
