program zen;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  zeus.zen.commandline in 'zeus.zen.commandline.pas',
  zeus.zen.commands.init in 'zeus.zen.commands.init.pas',
  zeus.zen.commands.use in 'zeus.zen.commands.use.pas',
  zeus.zen.commands.remove in 'zeus.zen.commands.remove.pas',
  zeus.zen.commands.update in 'zeus.zen.commands.update.pas',
  zeus.zen.commands.learn in 'zeus.zen.commands.learn.pas',
  zeus.zen.commands.install in 'zeus.zen.commands.install.pas';

begin
  try
    TCommandLine.Execute;
    ReadLn;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
