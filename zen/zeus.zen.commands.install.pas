unit zeus.zen.commands.install;

interface

implementation

uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenInstallCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenInstallCmd }

function TZenInstallCmd.Description: string;
begin
  Result := 'Reset your designtime packages to conform to the zen project configuration';
end;

function TZenInstallCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('install', TZenInstallCmd);


end.
