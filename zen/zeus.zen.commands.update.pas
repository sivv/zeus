unit zeus.zen.commands.update;

interface

implementation


uses
  System.SysUtils,
  System.Classes,
  zeus.zen.commandline;

type
  TZenUpdateCmd = class(TCommandHandler)
  public
    function Help : string; override;
    function Description : string; override;
  end;

{ TZenUpdateCmd }

function TZenUpdateCmd.Description: string;
begin
  Result := 'Updates submodules to the latest compatible commits for your project and recursive subprojects';
end;

function TZenUpdateCmd.Help: string;
begin

end;

initialization
  TCommandLine.RegisterCommand('update', TZenUpdateCmd);




end.
